package com.example.reservation.controller;

import com.example.reservation.dao.ArticleRepository;
import com.example.reservation.exception.ResourceNotFoundException;
import com.example.reservation.models.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ArticleController {
    @Autowired
    private ArticleRepository articleRepository;
    

    @GetMapping("/articles")
    public List<Article> getArticles(){
        return articleRepository.findAll();
    }

    /*@PostMapping(value = "/salles/{salleId}/article")
    public Article addArticle(@PathVariable(value = "salleId") Long salleId ,@RequestBody Article article){
        for(Article res: articleRepository.findAll()){

            if(res.getDate().equals(article.getDate()) && res.getSalle().getId().equals(salleId)){
                throw new ResponseStatusException(HttpStatus.IM_USED);
            }
        }
        return salleRepository.findById(salleId).map(salle -> {
            article.setSalle(salle);
            return articleRepository.save(article);
        }).orElseThrow(() -> new ResourceNotFoundException("salle id" + salleId));
    }
*/
    @DeleteMapping(value = "/articles/{articleId}")
    public Map<String, Boolean> deleteArticle(@PathVariable(value = "articleId") Long id) throws ResourceNotFoundException{
        Article article = articleRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Article not found ::" + id));

        articleRepository.delete(article);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;

    }



}
