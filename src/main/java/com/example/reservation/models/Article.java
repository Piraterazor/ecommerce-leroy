package com.example.reservation.models;

import javax.persistence.*;

@Entity
@Table(name = "article")
public class Article {

    @Column(name = "id_article")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idArticle;
    @Column(name = "nom_article")
    private String nomArticle;
    @Column(name = "categorie_article")
    private String categorieArticle;
    @Column(name = "prix_article")
    private float prixArticle;

    public int getIdArticle() {
        return idArticle;
    }

    public String getNomArticle() {
        return nomArticle;
    }

    public void setNomArticle(String nomArticle) {
        this.nomArticle = nomArticle;
    }

    public String getCategorieArticle() {
        return categorieArticle;
    }

    public void setCategorieArticle(String categorieArticle) {
        this.categorieArticle = categorieArticle;
    }

    public float getPrixArticle() {
        return prixArticle;
    }

    public void setPrixArticle(float prixArticle) {
        this.prixArticle = prixArticle;
    }
}
